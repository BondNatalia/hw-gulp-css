const {src, dest, watch, parallel,series} = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const concat = require("gulp-concat");
const uglify = require("gulp-uglify-es").default;
const browserSync = require("browser-sync");
// const parallel = require("bach");
const autoprefixer = require('gulp-autoprefixer');
const imagemin = require('gulp-imagemin');
const del = require('del');
const cleancss = require('gulp-clean-css');
const minifyjs = require("gulp-js-minify");

const srcPath = "src/";
    const distPath = "dist/";
const paths = {
    styles: {
        src: `${srcPath}scss/**/*.scss`,
        dest: `${distPath}css/`
    },
    scripts: {
        src: `${srcPath}js/**/*.js`,
        dest: `${distPath}js/`
    },
    html:{
        src:`${srcPath}index.html`,
        dest:distPath
    },
    img:{
        src:`${srcPath}image/**/*.*`,
        dest: `${distPath}image/`
    }
};
// convert Style

function styles() {
    // return src("src/scss/style.scss")
    return src(paths.styles.src)
        .pipe(sass({ outputStyle: "compressed"}))
        .pipe(autoprefixer({
            cascade:false
        }))
        .pipe(concat("style.min.css"))
        // .pipe(dest("src/css"))
        .pipe(dest(paths.styles.dest))
        .pipe(browserSync.stream());

}
function scripts() {
    return src([
        "node_modules/jquery/dist/jquery.js",
        // "src/js/app.js"
        paths.scripts.src
    ])
        .pipe(concat("app.min.js"))
        .pipe(uglify())
        // .pipe(dest("src/js"))
        .pipe(dest(paths.scripts.dest))
        .pipe(browserSync.stream());
}
function watching() {
    // watch(["src/scss/**/*.scss"],styles);
    watch(["paths.styles.src"],styles);
    watch(["paths.scripts.src"],scripts);
    watch(["paths.scripts.src"],minifyJS);
    // watch(["src/js/app.js","!src/js/app.js"], scripts);
    // watch(["src/*.html"]).on("change",browserSync.reload)
    watch(["paths.html.src"],html);
}
function reloadPage() {
    browserSync.init({
        server:{
            // baseDir:"src/",
            baseDir:distPath,
            port:3000,
            keepalive:true
        }
    })
}
function imageMin(){
    // return src("src/image/")
    return src(paths.img.src)
        .pipe(imagemin())
        // .pipe(dest("src/image"));
        .pipe(dest(paths.img.dest));
}
function html() {
    return src(paths.html.src)
        .pipe(dest(paths.html.dest))
        .pipe(browserSync.stream());
}
function delDist() {
    return del(distPath);
}
function minifyJS() {
    return src(paths.scripts.dest)
        .pipe(minifyjs())
        .pipe((dest(paths.scripts.dest)));
}
function cleanCSS() {
    return src(paths.styles.dest)
        .pipe(cleancss())
        .pipe(dest(paths.styles.dest));
}
exports.styles = styles;
exports.scripts = scripts;
exports.watching = watching;
exports.reloadPage = reloadPage;
exports.imageMin = imageMin;
exports.delDist = delDist;
exports.minifyJS = minifyJS;
exports.cleanCSS = cleanCSS;

exports.default = series(delDist,parallel(reloadPage, watching, scripts, imageMin, html, styles));
exports.build = series(delDist,parallel (styles,cleanCSS, scripts, minifyJS, imageMin, html));
exports.dev = series(watching, reloadPage);
